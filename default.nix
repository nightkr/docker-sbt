{pkgs ? import <nixpkgs> {}}:
pkgs.lib.mapAttrs (tag: jdk:
let
  sbt = pkgs.sbt.override { jre = jdk; };
in pkgs.dockerTools.buildImage {
  inherit tag;
  name = "registry.gitlab.com/teozkr/docker-sbt";
  contents = map (pkg: pkgs.symlinkJoin {
    name = "${pkg.name}-symlinkJoin";
    paths = pkg;
  }) [
    pkgs.bashInteractive
    pkgs.coreutils
    pkgs.gnused pkgs.gnugrep pkgs.gawk pkgs.rsync
    jdk
    sbt
  ];
  runAsRoot =
    ''
      ${pkgs.dockerTools.shadowSetup}
      # sbt refuses to run if /tmp is missing
      mkdir -p /tmp
    '';
}) {
  inherit (pkgs) openjdk8 openjdk10;
}
